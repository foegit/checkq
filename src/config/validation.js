// validation configuration

export default {
  username: {
    minLength: 3,
    maxLength: 10,
  },
  password: {
    minLength: 6,
    maxLength: 20,
  }
}
