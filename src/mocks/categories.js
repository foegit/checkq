// list of gategories

const data = [
  {
    name: 'Алгебра і геометрія',
    id: 1,
    countOfTest: 145,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Бази даних',
    id: 2,
    countOfTest: 155,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Програмування',
    id: 3,
    countOfTest: 101,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Дискретна математика',
    id: 4,
    countOfTest: 188,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Архітектура обчислюваних систем',
    id: 5,
    countOfTest: 125,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Диференційні рівняння',
    id: 6,
    countOfTest: 159,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Теорія ймовірності',
    id: 7,
    countOfTest: 119,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Чисельні методи',
    id: 8,
    countOfTest: 157,
    avatar: 'https://picsum.photos/400/300'
  },
  {
    name: 'Математичний аналіз',
    id: 9,
    countOfTest: 157,
    avatar: 'https://picsum.photos/400/300'
  },
]

export default data;
